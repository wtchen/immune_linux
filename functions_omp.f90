module param
    use mkl_vsl_type
    use mkl_vsl
    implicit none

    integer :: parasite_num, parasite_compl, complexity, init_rcpt_scale, compare_size, pop_size

    integer(1) :: compl_slot = 1, rcpt_slot = 2, spec_slot = 3
    !*******************!
    real(16) :: chance_adjust, mutation_rate
    integer :: rcpt_mut_div, spec_mut_amp
    integer :: nthread = 1
    logical :: fix_spec
    integer(1) :: spec_fixed
    end module param

module quick_sort
use param
    contains

    recursive subroutine quicksort(pop, left, right)
    implicit none
    integer :: pop(:,:)
    integer :: length, left, right, pv
    if (right > left)then
        pv = right
        call partition(pop, left, right, pv)
        call quicksort(pop, left, pv-1)
        call quicksort(pop, pv + 1, right)
        end if
    end subroutine quicksort

    subroutine partition(pop, left, right, pv)
    integer, intent(out) :: pop(:,:)
    integer, intent(out) :: pv
    integer, intent(in) :: left, right
    integer :: i, j
    integer, allocatable :: pivot(:)

    allocate(pivot(size(pop,dim=1)))
    pivot = pop(:, right)
    i = left
    do j = left, right - 1
        if(pop(rcpt_slot, j) .GE. pivot(rcpt_slot))then
            call swap(pop(:, i), pop(:, j))
            i = i + 1
        end if
    end do
    call swap(pop(:, i), pop(:, right))
    pv = i
    end subroutine partition

    subroutine swap(tr1, tr2)
    integer, intent(out) :: tr1(:), tr2(:)
    integer, allocatable :: tmp(:)

    allocate(tmp(size(tr1)))
    tmp = tr1
    tr1 = tr2
    tr2 = tmp
    end subroutine swap

end module quick_sort

module bit_func
    use param
    contains

        subroutine rand_intmkl(stream, n, r)
            integer, intent(in):: n
            TYPE (VSL_STREAM_STATE), intent(out) :: stream
            integer(8), intent(out), dimension(n) :: r
            integer :: method, errcode
            method = VSL_RNG_METHOD_UNIFORMBITS64_STD
            errcode = virnguniformbits64(method, stream, n, r)
        end subroutine rand_intmkl

        pure logical function match(int1, int2, threshold)result(res)
            integer(8), intent(in) :: int1, int2
            integer, intent(in):: threshold
            integer(8) :: sum, tmp, mask
            !sum = 0
            mask = maskr(compare_size, 8)
            tmp = NOT(IEOR(int1, int2))
            sum = popcnt(IAND(mask, tmp))
            !do j=1, compare_size
            !    sum = sum + IAND(#0000000000000001, tmp)
             !   tmp = ISHFT(tmp, -1)
           ! end do
            res = sum >= threshold
        end function match

        integer function rand_rcpt_num(stream, scale)result(res)
            integer :: scale
            TYPE (VSL_STREAM_STATE), intent(out) :: stream
            integer(8) :: r(1)
            call rand_intmkl(stream, 1, r)
            res = abs(mod(r(1), scale))
        end function rand_rcpt_num

        integer function rand_spec_num(stream)result(res)
            integer :: scale
            TYPE (VSL_STREAM_STATE), intent(out) :: stream
            integer(8) :: r(1)
            scale = compare_size - 19
            call rand_intmkl(stream, 1, r)
            res = abs(mod(r(1), scale)) + 18
        end function rand_spec_num
end module bit_func

module doa
    use bit_func
    use param
       contains

    subroutine test_ind(stream, complexity, rcpt_num, specificity, res, eff_rcpt_num)
        TYPE (VSL_STREAM_STATE), intent(out) :: stream
        integer :: rcpt_num, complexity, specificity
        integer :: i, j, k, m, index
        integer, intent(out) :: eff_rcpt_num
        logical, allocatable :: hits(:)
        logical, intent(out):: res
        integer(8), allocatable, dimension(:) :: receptors, eff_receptors, somatics
        logical, allocatable, dimension(:) :: self_flag, rcpt_flag
        integer(4), allocatable, dimension(:) :: rcpt_count
        integer(8), dimension(parasite_num * parasite_compl) :: parasites


        !*****************make receptors*******************!
        allocate(somatics(complexity))
        allocate(self_flag(complexity))
        call rand_intmkl(stream, complexity, somatics)
        allocate(receptors(rcpt_num))
        allocate(rcpt_flag(rcpt_num))
        allocate(rcpt_count(rcpt_num))
        rcpt_count = 0
        self_flag = .true.
        call rand_intmkl(stream, rcpt_num, receptors)
        do i = 1, rcpt_num
            forall(j = 1: complexity)self_flag(j) = match(somatics(j), receptors(i), specificity)
            rcpt_flag(i) = .NOT.ANY(self_flag)
        end do
        deallocate(somatics)
        deallocate(self_flag)
        rcpt_count = 0
        forall(i = 1:rcpt_num, rcpt_flag(i))rcpt_count(i) = 1
        eff_rcpt_num = sum(rcpt_count)
        !print *, eff_rcpt_num
        if(eff_rcpt_num < 1)then
            res = .true.
            return
        end if

        allocate(eff_receptors(eff_rcpt_num))

        j = 1
        do i=1,rcpt_num
            if(rcpt_flag(i))then
                eff_receptors(j) = receptors(i)
                j = j + 1
            end if
        end do
        deallocate(rcpt_flag)
        deallocate(rcpt_count)
        deallocate(receptors)
        !***************************************!

        allocate(hits(parasite_num))

        hits=.false.
        call rand_intmkl(stream, parasite_num * parasite_compl, parasites)
        !!$OMP PARALLEL PRIVATE(i,j,k)
        !!DIR$ PARALLEL ALWAYS
        !!$OMP DO SCHEDULE(GUIDED, 50)
            outer: do i = 1, parasite_num
                inner: do j = 1, parasite_compl
                do k = 1, eff_rcpt_num
                    index = (i - 1) * parasite_compl + j
                    if(match(parasites(index), eff_receptors(k), specificity))then
                        hits(i) = .true.
                        exit inner
                    end if
                end do
                end do inner
        end do outer
        !!$OMP END DO NOWAIT
        !!$OMP END PARALLEL

        !res = .false.
        res = .not.ALL(hits)

        deallocate(eff_receptors)
        deallocate(hits)
        end subroutine test_ind

    end module doa

module reproduction
use param
    contains
    pure integer(2) function cmp_function(a1, a2)
        real(16), intent(in) :: a1, a2
        real(16) :: diff
        diff= a1 - a2
        if(diff > 0.)then
            cmp_function = -1 !To make descending sort result
        else if(diff < 0.)then
            cmp_function = 1
        else
            cmp_function = 0
        end if
    end function cmp_function

    pure function make_score(rcpt_num_seq)result(scores)
        real(16), allocatable :: scores(:)
        real(16) :: sum_chance
        integer, intent(in) :: rcpt_num_seq(:)
        integer :: i, j, length

        length = size(rcpt_num_seq)

        allocate(scores(length))
        !print *, rcpt_num_seq
        forall(i = 1:length)scores(i) = 1. / ((rcpt_num_seq(i) + 1.) ** chance_adjust)
        sum_chance = sum(scores)
        forall(j = 1: length)scores(j) = scores(j) / sum_chance
        !print *, "?????"
    end function make_score


    pure integer function find_score(rank_seq, rnd_real)result(res)
        real(16), intent(in) :: rank_seq(0:)
        real(8), intent(in) :: rnd_real
        integer :: bot, top, tmp

        bot = 1
        top = size(rank_seq) - 1
        do while(bot < top - 1)
            tmp = (top + bot) / 2
            if(rank_seq(tmp) < rnd_real)then
                bot = tmp
            else
                top = tmp
            end if
        end do
        res = top
    end function find_score

    pure function make_chance_seq(scores)result(chance_seq)
    real(16), allocatable :: chance_seq(:)
    real(16), intent(in) :: scores(:)
    integer :: i, length

    length = size(scores)
    allocate(chance_seq(0:length))
    chance_seq(0) = 0.
    do i = 1, length
        chance_seq(i) = chance_seq(i-1) + scores(i)
    end do
    end function make_chance_seq

    subroutine reproduce(stream, pop, surv_pop, chance_seq)
    TYPE (VSL_STREAM_STATE), intent(out) :: stream
    integer, intent(out) :: pop(:, :)
    integer, intent(in) ::  surv_pop(3,:)
    real(16), intent(in) :: chance_seq(:)
    real(8), allocatable:: rnd_real(:), rcpt_chance(:), spec_chance(:), rcpt_range(:)
    integer :: i,j, status, method, tmpslot, tmp_ind(3)
    integer, allocatable :: reprod_list(:)

    allocate(rnd_real(pop_size))
    allocate(rcpt_chance(pop_size))
    allocate(spec_chance(pop_size))
    allocate(reprod_list(pop_size))
    allocate(rcpt_range(pop_size))

    method = VSL_RNG_METHOD_UNIFORM_STD_ACCURATE
    status = vdrnguniform( method, stream, pop_size, rnd_real, 0._8, 1._8 )
    status = vdrnguniform( method, stream, pop_size, rcpt_chance, 0._8, 1._8 )
    status = vdrnguniform( method, stream, pop_size, spec_chance, 0._8, 1._8 )
    status = vdrnguniform( method, stream, pop_size, rcpt_range, -1._8, 1._8 )

    do i = 1, pop_size
        tmpslot = find_score(chance_seq, rnd_real(i))
        tmp_ind = surv_pop(:, tmpslot)
        if (rcpt_chance(i) < mutation_rate)then
            tmp_ind(rcpt_slot) = tmp_ind(rcpt_slot) + rcpt_range(i) * tmp_ind(rcpt_slot) * exp(-1. * rcpt_mut_div)
        end if
        if(fix_spec)then
            tmp_ind(spec_slot) = spec_fixed
        else if (spec_chance(i) < mutation_rate / 2.)then
            tmp_ind(spec_slot) = tmp_ind(spec_slot) + spec_mut_amp
        else if (spec_chance(i) < mutation_rate)then
            tmp_ind(spec_slot) = tmp_ind(spec_slot) - spec_mut_amp
        end if

        pop(:,i) = tmp_ind

    end do
    end subroutine reproduce




end module reproduction

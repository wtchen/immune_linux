program testf2008
    implicit none
    integer(8) :: x
    integer :: y

    read *, x
    y = popcnt(x)
    print *, "popcnt(x)=", y
    y =  leadz(x)
    print *, "leadz(x)=", y
    y = poppar(x)
    print *, "poppar(x)=", y
    y = trailz(x)
    print *, "trailz(x)=", y
end


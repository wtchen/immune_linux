! programme simulation of the evolution of (adaptive) immune system
include 'functions_v.f90'
!include 'random.f90'
!include 'MersenneTwister.f90'
include 'mkl_vsl.f90'

program main
    USE IFPORT
    USE bit_func
    !USE random_dist
    USE doa
    USE quick_sort
    USE reproduction
    !USE MersenneTwister_mod

    implicit none
    !type(randomNumberSequence) :: randomNumbers
    real :: version = 0.1
    integer :: i,j, loop_count, generation_num
    integer, allocatable :: eff_rcpt_num(:)
    !logical, dimension(2,50000) :: l
    real :: time1, time2
    integer, allocatable :: population(:,:)
    real(16), allocatable :: rank_seq(:), chance_seq(:)
    logical, allocatable :: doa_table(:)
    integer, allocatable :: survivors(:,:)
    character(len=25) :: arg
    integer :: NARGS, recept
    integer(1) :: arg_num_flag

    !**************************************************!
      TYPE (VSL_STREAM_STATE) :: stream
      integer(kind=4) errcode
      integer brng, seedmkl, n
      integer, allocatable :: surv_flag(:)
      integer(4) :: surv_num

      !!*******************************************************************!!
      !!                             BODY                                  !!
      !!*******************************************************************!!
      generation_num = 1500
      pop_size=1000
      complexity = 100
      parasite_num = 10
      parasite_compl =5
      init_rcpt_scale = 2000
      compare_size = 32
      seedmkl = 432535
      chance_adjust = 2.
      mutation_rate = 0.01
      rcpt_mut_div = 20
      spec_mut_amp = 1

      NARGS = IARGC()
      !print *, NARGS
      arg_num_flag = 0
      do i = 1, command_argument_count()
          call get_command_argument(i, arg)
          if(arg_num_flag .eq. 0)then
              select case (arg)
              case ("-v", "--version")
                  print *, "Immune system evolution simulation version", version
                  stop
              case ("-h", "--help")
                  call help_subr
                  stop
              case ("-p", "--population_size")
                  arg_num_flag = 1
              case ("-g", "--generation")
                  arg_num_flag = 2
              case ("-c", "--complexity")
                  arg_num_flag = 3
              case ("-n", "--parasite_number")
                  arg_num_flag = 4
              case ("-x", "--parasite_complexity")
                  arg_num_flag = 5
              case("-r", "--initial_repertoire_scale")
                  arg_num_flag = 6
              case ("-b", "--bit_match_size")
                  arg_num_flag = 7
              case ("-s", "--seed")
                  arg_num_flag = 8
              case ("-a", "--rcpt_size_penalty")
                  arg_num_flag = 9
              case ("-m", "--mutation_rate")
                  arg_num_flag = 10
              case ("-f", "--rcpt_mutation_fraction")
                  arg_num_flag = 11
              !case ("-nthread", "--number_of_threads")
                  !arg_num_flag = 12
              case default
                print '(a,a,/)', 'Unrecognized command-line option: ', arg
                call help_subr
                stop
              end select
          else
              select case (arg_num_flag)
              case (1)
                  read(arg, *) pop_size

              case (2)
                  read(arg, *) generation_num

              case(3)
                  read(arg, *) complexity

              case (4)
                  read(arg, *) parasite_num

              case (5)
                  read(arg, *) parasite_compl
              case (6)
                  read(arg, *) init_rcpt_scale

              case (7)
                  read(arg, *) compare_size

              case (8)
                  read(arg, *) seedmkl

              case (9)
                  read(arg, *) chance_adjust

              case (10)
                  read(arg, *) mutation_rate

              case (11)
                  read(arg, *) rcpt_mut_div

              !case (12)
                !read(arg, *) nthread

              case default
                print '(a,a,/)', 'Unrecognized command-line option: ', arg
                call help_subr
                stop

              end select
              arg_num_flag = 0
              end if
              end do

      !**************************************************!
      !call OMP_SET_NUM_THREADS(nthread)
      brng = VSL_BRNG_MT2203
      !seedmkl=96453
      n=1000
      errcode=vslnewstream( stream, brng,  seedmkl )

    !**************************************************!
      print *, "#############################################################"
      print *, "#An immune system evolution simulator version 0.1"
      !print *, "Author: Wentao Chen, Universite Joseph Fourier"
      print *, "#############################################################"
      print *, "#", "Number of generations to run:", generation_num
      print *, "#Program running with parameters:"
      print *, "#", "Seed for Mersenne twister random number generator =", seedmkl
      print *, "#", "Host population size =", pop_size
      print *, "#", "Host complexity =", complexity
      print *, "#", "Number of parasites per host per generation =", parasite_num
      print *, "#", "Parasite complexity =", parasite_compl
      print *, "#", "initial repertoire size scale =", init_rcpt_scale
      print *, "#", "Maximum number of bits to match =", compare_size
      print *, "#", "Repertoire size penalty =", chance_adjust
      print *, "#", "Mutation rate =", mutation_rate
      print *, "#", "Repertoire size mutation fraction", rcpt_mut_div

    call CPU_TIME(time1)
    allocate(eff_rcpt_num(pop_size))
    eff_rcpt_num = 0

    print *, "Generation    mean_repertoire_size    mean_effective_repertoire size  mean_specificity    surviving_individuals"
    allocate(population(3, pop_size))
    do i=1, pop_size
        population(compl_slot, i) = complexity
        population(rcpt_slot, i) = rand_rcpt_num(stream, init_rcpt_scale)
        population(spec_slot, i) = rand_spec_num(stream)
        !print *, population(:,i)
    end do

    !print *, "CHECKPOINT1"
    allocate(doa_table(pop_size))
    allocate(surv_flag(pop_size))
   !!****************************parasite attack-reproduciton loop***************************!!
    main_loop : do loop_count = 1, generation_num
    !print *, "checkpoint", loop_count

    !DIR$ PARALLEL ALWAYS
    do i = 1, pop_size
        call test_ind(stream, population(compl_slot, i), population(rcpt_slot, i), population(spec_slot, i), doa_table(i), eff_rcpt_num(i))
        !print *, i
    end do
    !print *, "checkpoint doa"
    !print *, doa_table
    surv_flag = 1
    where(doa_table)surv_flag = 0
    !print *, surv_flag
    surv_num = sum(surv_flag)

    !print *, surv_num

    if (surv_num < 1)then
        print *, "No survivors!"
        read (*,*)
        stop
    end if

    print *, loop_count, (sum(population(rcpt_slot, :)) * 1.)/ pop_size, (sum(eff_rcpt_num) * 1.)/ pop_size, (sum(population(spec_slot, :)) * 1.)/ pop_size, surv_num

    !print *, doa_table(1:100)
    !print *, surv_flag(1:100)


    allocate(survivors(3, surv_num))
    !print *, "checkpoint after alloc surv"

    survivors = 0
    j = 1
    do i = 1, pop_size
        if(.not.doa_table(i))then
            !print *, doa_table(i)
            !print *, i, j
            survivors(:, j) = population(:, i)
            j = j + 1
        end if
    end do
    !print *, survivors(rcpt_slot, 1:20)
    !print *, "checkpoint after surv"
    call quicksort(survivors, 1, surv_num)
    !print *, survivors(rcpt_slot, 1:20)
    !print *, "checkpoint after qs"
    allocate(rank_seq(surv_num))
    !print *, "checkpoint", loop_count
    allocate(chance_seq(0:surv_num))

    rank_seq = make_score(survivors(rcpt_slot, :))
    chance_seq = make_chance_seq(rank_seq)


    call reproduce(stream, population, survivors, chance_seq)


    !print *, (sum(population(rcpt_slot, :)) * 1.)/ pop_size, (sum(population(spec_slot, :)) * 1.)/ pop_size
    deallocate(survivors)
    deallocate(rank_seq)
    deallocate(chance_seq)
    end do main_loop

    call CPU_TIME(time2)
    print *, "#CPU time:", time2 - time1, "s"
    !print *, population(rcpt_slot, 1:10)
    !print *, rank_seq(1:10)
    read (*,*)
      contains
      subroutine help_subr()
      print '(a)', "######## Immune system evolution simulator ########"
      print '(a)', "Usage: simulation [OPTIONS]"
      print '(a)', 'Options:'
      print *, "-v  --version    print version and exit"
      print *, "-h  --help   print usage information and exit"
      print *, "-p  --population_size (1 arg):  population size, default:", pop_size
      print *, "-g  --generation (1 arg) : number of generations to run, default:", generation_num
      print *, "-c  --complexity (1 arg) : host complexity, default:", complexity
      print *, "-n  --parasite_number (1 arg) : parasite number per host per generation, default:", parasite_num
      print *, "-x  --parasite_complexity (1 arg) : parasite complexity, default:", parasite_compl
      print *, "-r  --initial_repertoire_scale (1 arg) : initial repertoire size scale, default:", init_rcpt_scale
      print *, "-b  --bit_match_size (1 arg) : maximum number of bits to match, default:", compare_size
      print *, "-s  --seed (1 arg) : Seed for the Mersenne twister random number generator, default", seedmkl
      print *, "-a  --rcpt_size_penalty (1 arg) : degree of reproduction penalty on repertoire size, default:", chance_adjust
      print *, "-m  --mutation_rate (1 arg) : mutation rate, default:", mutation_rate
      print *, "-f  --rcpt_mutation_fraction (1 arg) : fraction of the repertoire size in muatation, default:", rcpt_mut_div
      !print *, "-nthread --number_of_threads : number of threads in parallel, default:", nthread
      end subroutine help_subr
    end
